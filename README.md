# Introdução
	VueJS é um famoso framework de desenvolvimento web. Seu uso é em grande escala em repositórios open-source e close-source por conta de sua estabilidade e facilidade de uso. Porém, uma das maiores dificuldades de desenvolvedores front-end é trabalhar com CSS. Para facilitar nosso trabalho, iremos utilizar a biblioteca Bootstrap, do Twitter.
	O objetivo do presente trabalho é construir uma simples aplicação com VueJS + NPM + Bootstrap que inverta uma string binária digitada pelo usuário. Essa aplicação possui todos os requisitos solicitados, como componentes VUE, uso de um gerenciador de pacotes NPM, uso de Bootstrap e seu código fonte foi integrado usando a ferramenta Codeberg.
# Setup do projeto
Para começar o trabalho, primeiro começamos criando o repositório, com o comando npm init vue@latest. Ao criar os primeiros arquivos, nós vemos já uma sequência predefinida de pastas e arquivos. Isso se dá pelo fato de que o VueJS opta pelo uso da arquitetura MVC (model, view e controller).
O primeiro passo da construção de nossa aplicação foi remover os arquivos desnecessários, realizando assim uma limpeza no nosso código. Então, no arquivo index.html na pasta raíz, colocamos nosso código.
	Para clonar a aplicação, basta digitar git clone https://codeberg.org/Luiz.Rosa8/TDS-FinalWork.git. Para instalar, basta digitar npm install. Para rodar nossa aplicação, basta digitar npm run dev.
# O código
	O projeto possui um arquivo .html que possui o código com o script em .vue e os styles em .css.
	Declaramos uma aplicação #app e dentro dela, duas variáveis strings, a binaryString e a negatedBinary. Também possui dois métodos definidos, que são o onInputChange e o negateBinary.
	Quando o usuário digitar algo na string, esse valor passa por uma regex que valida 0 e 1. Caso tenha apenas 0 e 1 na string, exibimos a string negada logo abaixo. Para caso tenha algum valor que difere destes, exibimos "Invalid binary string".
	O bootstrap foi utilizado para a estilização dos labels, títulos e inputs.
	O código se encontra em: https://codeberg.org/Luiz.Rosa8/TDS-FinalWork.
